"""Test the Hypergeometric random vaiable"""

import numpy as np
import tensorflow as tf
from tensorflow_probability.python.internal import test_util
from gemlib.distributions.hypergeometric import Hypergeometric
import tensorflow_probability as tfp

tfd = tfp.distributions


@test_util.test_all_tf_execution_regimes
class TestHypergeometric(test_util.TestCase):
    def setUp(self):
        self._rng = np.random.RandomState(5)
        super(TestHypergeometric, self).setUp()

    def test_neg_args(self):
        """Test for invalid arguments"""
        with self.assertRaisesRegex(
            ValueError, "N, K, and n must be non-negative"
        ):
            self.evaluate(Hypergeometric(N=-3, K=1, n=2, validate_args=True))

    def test_sample_n_float32(self):
        """Sample returning float32 args"""

        fixture = np.array(
            [[7, 12, 12], [10, 11, 12], [10, 16, 9]], dtype=np.float32
        )
        X = Hypergeometric(345.0, 35.0, 100.0)
        x = X.sample([3, 3], seed=1)

        x = self.evaluate(x)
        self.assertDTypeEqual(x, np.float32)
        self.assertAllEqual(x, fixture)

    def test_sample_n_float64(self):
        """Sample returning float32 args"""

        fixture = np.array(
            [[7, 8, 12], [8, 8, 13], [12, 11, 10]], dtype=np.float64
        )
        X = Hypergeometric(
            np.float64(345.0), np.float64(35.0), np.float64(100.0)
        )
        x = X.sample([3, 3], seed=1)

        x = self.evaluate(x)
        self.assertDTypeEqual(x, np.float64)
        self.assertAllEqual(x, fixture)
