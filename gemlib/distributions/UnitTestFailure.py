from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# Dependency imports

import tensorflow as tf
import tensorflow_probability as tfp
from tensorflow_probability.python.internal import test_util


@test_util.test_all_tf_execution_regimes
class TestDiscreteTimeStateTransitionModel(test_util.TestCase):

    def init_model(self, pars):
        loc, scale = tf.unstack(pars)
        return tfp.distributions.Normal(loc, scale)

    def test_normal_lp(self):
        def logp(pars):
            mod = self.init_model(pars)
            return mod.log_prob(event)

        dtype = tf.float32
        pars = tf.constant([0.0, 1.0], dtype)
        nor_orig = self.init_model(pars)
        #event = self.evaluate(nor_orig.sample())
        event = nor_orig.sample()
        lp_actual = nor_orig.log_prob(event)
        # lp_maxima = nor_orig.log_prob(event)
        # lp_maxima = logp(pars)

        coords = tf.constant([[x, y ] for x in range(-6, 6, 2) for y in range(1, 11, 2)], dtype=dtype)
        lps = tf.map_fn(fn=logp, elems=coords)
        lp_maxima = tf.math.reduce_max(lps)

        # print('lp_actual, lp_maxima: ', lp_actual, lp_maxima)
        self.assertAllClose(lp_maxima, lp_actual, rtol=1e-06, atol=1e-06)

if __name__ == '__main__':
    tf.test.main()
