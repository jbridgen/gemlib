# gemlib

`gemlib` is a library providing Python classes for epidemic
modelling. 

`gemlib` provides a flexible suite of building
blocks for assembling complex state-transition-model based 
analyses as well as MCMC algorithms for marginalising out censored 
transition events.  It is currently designed to be compatible with [TensorFlow 
Probability](https://tensorflow.org/probability), with `distributions`
and `mcmc` submodules layed out in a similar fashion.  However, we
expect to depart from being an "extension" of TFP to a full abstraction
in the future. 

This documentation currently provides both rudimentary tutorials and
the library reference documentation.

```{toctree}
:maxdepth: 2
:caption: Contents

tutorials.md
modules.rst
```
