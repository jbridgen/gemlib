"""Test event time samplers"""

import pytest

import tensorflow as tf

from gemlib.mcmc.event_time_mh import UncalibratedEventTimesUpdate


@pytest.fixture
def random_events():
    """SEIR model with prescribed starting conditions"""
    events = tf.random.uniform(
        [10, 10, 3], minval=0, maxval=100, dtype=tf.float64, seed=0
    )
    return events


@pytest.fixture
def initial_state():
    popsize = tf.fill([10], tf.constant(100.0, tf.float64))
    initial_state = tf.stack(
        [
            popsize,
            tf.ones_like(popsize),
            tf.zeros_like(popsize),
            tf.zeros_like(popsize),
        ],
        axis=-1,
    )
    return initial_state


def test_uncalibrated_event_time_update(random_events, initial_state):
    def tlp(events):
        return tf.constant(0.0, tf.float64)

    kernel = UncalibratedEventTimesUpdate(
        target_log_prob_fn=tlp,
        target_event_id=1,
        prev_event_id=0,
        next_event_id=2,
        initial_state=initial_state,
        dmax=4,
        mmax=1,
        nmax=10,
    )

    pkr = kernel.bootstrap_results(random_events)

    new_state, results = kernel.one_step(random_events, pkr, seed=0)

    print(results)

    assert results.m == 5
    assert results.t == 8
    assert results.delta_t == -4
    assert results.x_star == 4
    assert results.target_log_prob == 0.0
