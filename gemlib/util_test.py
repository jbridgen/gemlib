"""Test `gemlib` utility functions."""

import pytest

import numpy as np

from gemlib.util import states_from_transition_idx


@pytest.fixture
def svir_stoichiometry():
    """Fixture for SVIR incidence matrix."""
    return np.array(
        [
            # S  V  I  R
            [-1, 0, 1, 0],  # S->I
            [-1, 1, 0, 0],  # S->V
            [0, -1, 1, 0],  # V->I
            [0, 0, -1, 1],  # I->R
        ]
    )


def test_states_from_transition_idx(svir_stoichiometry):
    """Ensure source and destination enums are correct."""
    # S->I
    assert states_from_transition_idx(0, svir_stoichiometry) == (0, 2)
    # S->V
    assert states_from_transition_idx(1, svir_stoichiometry) == (0, 1)
    # V->I
    assert states_from_transition_idx(2, svir_stoichiometry) == (1, 2)
    # I->R
    assert states_from_transition_idx(3, svir_stoichiometry) == (2, 3)
