"""Test marginalised likelihood function"""

import tensorflow as tf

from gemlib.distributions.discrete_time_state_transition_marginal import (
    marginal_discrete_markov_log_prob,
)


class TestStochasticMarginal:
    def test_stochastic_marginal_logp(self, simple_sir_metapop_epidemic):

        h0_priors = dict(concentration=[0.1, 2.0], rate=[0.1, 4.0])
        logp = marginal_discrete_markov_log_prob(
            events=simple_sir_metapop_epidemic["events"],
            initial_state=simple_sir_metapop_epidemic["initial_state"],
            initial_step=0,
            time_delta=1.0,
            gpriors=h0_priors,
            hazard_fn=simple_sir_metapop_epidemic["hazard_fn"],
            stoichiometry=simple_sir_metapop_epidemic["stoichiometry"],
        )
        assert abs(logp - -127.9736091) < 1.0e-6
