# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# Path setup

import os
import sys

sys.path.insert(0, os.path.abspath("../.."))


# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'gemlib'
copyright = '2023, Chris Jewell, Alison Hale'
author = 'Chris Jewell, Alison Hale'
release = '0.8.4.1'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.autosummary",
    "sphinx.ext.inheritance_diagram",
    "sphinx.ext.graphviz",
    "nbsphinx",
    "myst_parser",
    "sphinx.ext.napoleon",
]

templates_path = ['_templates']
exclude_patterns = []

source_suffix = [".rst", ".md"]

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "sphinx_book_theme"
html_title = project
html_theme_options = {
    "repository_url": "https://gitlab.com/gem-epidemics/gemlib",
    "use_repository_button": True,
}
html_logo = "images/gem-logo.png"
html_favicon = "images/favicon.ico"

pygments_style = "friendly"
html_static_path = ['_static']

# Autosummary configuration
autosummary_generate = True
autoclass_content = "class"

nitpicky = False

graphviz_output_format = "svg"
