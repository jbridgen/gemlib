"""Test left-censored proposal mechanism"""

import numpy as np

from gemlib.mcmc.left_censored_events_proposal import (
    LeftCensoredEventTimeProposal,
)


def test_InitialConditionsEventTimeProposal(sir_metapop_example):

    proposer = LeftCensoredEventTimeProposal(
        events=sir_metapop_example["events"],
        initial_state=sir_metapop_example["initial_conditions"],
        transition=np.int32(0),
        stoichiometry=sir_metapop_example["stoichiometry"],
        num_units=1,
        max_timepoint=5,
        max_events=5,
    )

    for i in range(100):
        proposal = proposer.sample()

    assert proposal["unit"].shape == (1,)
    assert proposal["timepoint"].shape == (1,)
    assert proposal["direction"].shape == ()
    assert proposal["num_events"].shape == (1,)

    logp = proposer.log_prob(proposal)
    assert logp
