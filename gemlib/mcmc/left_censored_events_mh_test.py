"""Test initial event times MH"""

import numpy as np

from gemlib.mcmc.left_censored_events_mh import _update_state

from gemlib.mcmc.left_censored_events_mh import (
    UncalibratedLeftCensoredEventTimesUpdate,
)


def test__update_state_fwd(sir_metapop_example):

    initial_conditions = sir_metapop_example["initial_conditions"]
    events = sir_metapop_example["events"][:, :7, :]
    stoichiometry = sir_metapop_example["stoichiometry"]

    # Move SE events from past into present
    update_mock = dict(unit=[0], timepoint=[3], direction=0, num_events=[5])
    new_initial_conditions, new_events = _update_state(
        update=update_mock,
        current_state=(initial_conditions, events),
        transition_idx=0,
        stoichiometry=stoichiometry,
    )

    # Test events
    assert new_events.numpy()[0, 3, 0] == (events[0, 3, 0] + 5)

    intended_initial_conditions = np.array(
        [[1004, 45, 0], [500, 20, 0], [250, 10, 0]], dtype=np.float32
    )
    new_initial_conditions = new_initial_conditions.numpy()
    np.testing.assert_array_equal(
        new_initial_conditions, intended_initial_conditions
    )

    # Move IR events from present into past
    update_mock = dict(unit=[1], timepoint=[4], direction=1, num_events=[5])
    new_initial_conditions, new_events = _update_state(
        update=update_mock,
        current_state=(initial_conditions, events),
        transition_idx=1,
        stoichiometry=stoichiometry,
    )
    # Test events
    assert new_events.dtype == events.dtype
    assert new_initial_conditions.dtype == initial_conditions.dtype
    assert new_events.numpy()[1, 4, 1] == (events[1, 4, 1] - 5)

    intended_initial_conditions = np.array(
        [[999, 50, 0], [500, 15, 5], [250, 10, 0]], dtype=np.float32
    )
    new_initial_conditions = new_initial_conditions.numpy()
    np.testing.assert_array_equal(
        new_initial_conditions, intended_initial_conditions
    )


def test_LeftCensoredEventTimesUpdate(sir_metapop_example):

    kernel = UncalibratedLeftCensoredEventTimesUpdate(
        target_log_prob_fn=lambda ic, ev: np.float32(1.0),
        transition_index=0,
        stoichiometry=sir_metapop_example["stoichiometry"],
        max_timepoint=6,
        max_events=10,
    )

    current_state = (
        sir_metapop_example["initial_conditions"],
        sir_metapop_example["events"],
    )

    results = kernel.bootstrap_results(current_state)

    for i in range(100):
        current_state, results = kernel.one_step(
            current_state, results, results.seed
        )
    current_state = [x.numpy() for x in current_state]
    print("current_state[1]", current_state[1])

    assert np.all(current_state[0] >= 0.0)
    assert np.all(current_state[1] >= 0.0)
