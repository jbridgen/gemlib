Module index
=========================

Probability distributions
------------------------------

.. currentmodule:: gemlib.distributions
		   
.. autosummary::
    :toctree: distributions/

    DiscreteTimeStateTransitionModel
    Hypergeometric


MCMC kernels
--------------

.. currentmodule:: gemlib.mcmc

.. autosummary::
    :toctree: mcmc/
	      
    GibbsKernel
    CompoundKernel
    CBRKernel
    MultiScanKernel
    TransitionTopology
    UncalibratedEventTimesUpdate
    UncalibratedLeftCensoredEventTimesUpdate
    UncalibratedOccultUpdate

