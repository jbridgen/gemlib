`gemlib` scientific compute library
===========================================

`gemlib` is a scientific compute library build for epidemic 
analysis.  It forms a component of the [GEM](http://fhm-chicas-code.lancs.ac.uk/GEM/gem)
project aimed at developing a reusable domain-specific modelling
language for epidemic inference and simulation.

`gemlib` is heavily based on [Tensorflow Probability](https://www.tensorflow.org/probability), a 
probabilistic library for the [Tensorflow](https://www.tensorflow.org)
machine learning platform.  This package provide extensions for Tensorflow
Probability related to epidemic analysis.  
