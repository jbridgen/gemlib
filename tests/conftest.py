"""Standard test fixtures"""

import pytest
import tensorflow as tf

DTYPE = tf.float64


@pytest.fixture
def simple_sir_metapop_epidemic():
    """An epidemic of 3 metapopulations with a simple
    mixing matrix"""

    M = tf.constant([[0.0, 0.2, 0.1], [0.2, 0.0, 0.3], [0.1, 0.3, 0.0]], dtype=DTYPE)

    initial_state = tf.constant([[9, 1, 0], [10, 0, 0], [10, 0, 0]], dtype=DTYPE)
    stoichiometry = tf.constant([[-1, 1, 0], [0, -1, 1]], dtype=DTYPE)
    events = tf.constant(
        [
            [
                [2.0, 0.0],
                [5.0, 0.0],
                [2.0, 1.0],
                [0.0, 0.0],
                [0.0, 0.0],
                [0.0, 0.0],
                [0.0, 2.0],
                [0.0, 0.0],
                [0.0, 1.0],
                [0.0, 1.0],
                [0.0, 1.0],
                [0.0, 0.0],
                [0.0, 0.0],
                [0.0, 1.0],
                [0.0, 0.0],
            ],
            [
                [1.0, 0.0],
                [7.0, 1.0],
                [2.0, 0.0],
                [0.0, 2.0],
                [0.0, 1.0],
                [0.0, 0.0],
                [0.0, 3.0],
                [0.0, 0.0],
                [0.0, 0.0],
                [0.0, 0.0],
                [0.0, 0.0],
                [0.0, 1.0],
                [0.0, 0.0],
                [0.0, 1.0],
                [0.0, 0.0],
            ],
            [
                [1.0, 0.0],
                [6.0, 0.0],
                [3.0, 0.0],
                [0.0, 0.0],
                [0.0, 0.0],
                [0.0, 1.0],
                [0.0, 1.0],
                [0.0, 1.0],
                [0.0, 0.0],
                [0.0, 0.0],
                [0.0, 0.0],
                [0.0, 0.0],
                [0.0, 1.0],
                [0.0, 0.0],
                [0.0, 2.0],
            ],
        ],
        dtype=DTYPE,
    )

    def hazard_fn(t, state):
        S, I, R = range(state.shape[-1])
        si = 0.2 * (state[..., I] + 5.2 * tf.linalg.matvec(M, state[..., I]))
        ir = 0.1 * tf.ones(state.shape[-2], dtype=DTYPE)
        return [si, ir]

    return dict(
        hazard_fn=hazard_fn,
        initial_state=initial_state,
        events=events,
        stoichiometry=stoichiometry,
    )
