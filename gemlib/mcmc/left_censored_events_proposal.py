"""Implements a proposal for left-censored events"""

import tensorflow as tf
import tensorflow_probability as tfp
import tensorflow_probability.python.internal.prefer_static as ps

import gemlib.distributions as gld

from gemlib.util import compute_state
from gemlib.util import states_from_transition_idx

tfd = tfp.distributions


def _mask_max(x, t, axis=0):
    """Fills all elements where `i>t` along axis `axis`
    of `x` with `x.dtype.max`.
    """
    mask = tf.cast(tf.range(x.shape[axis]) > t, x.dtype) * x.dtype.max
    return x + mask


def LeftCensoredEventTimeProposal(
    events,
    initial_state,
    transition,
    stoichiometry,
    num_units,
    max_timepoint,
    max_events,
    dtype=tf.int32,
    name=None,
):
    """Propose to move `transition` events into or out of a randomly selected
    time-window [0, max_timepoint] (n.b. inclusive!) subject to bounds imposed by
    a state-transition process described by `stoichiometry`.  The event timeseries
    describes the number of events occuring along `R` transitions in `M` coupled
    units across `T` timepoints between `S` states.

    :param events: a [M, T, R] tensor describing number of events for each transition
                   in each unit at each timepoint.
    :param initial_state: a [M, S] tensor describing the initial values of the state
                          at the first timepoint.
    :param transition: the transition for which we wish to move events backwards or
                       forwards in time.
    :param stoichiometry: a [R, S] matrix describing the change in the value of each
                          state in response to an event along each transition.
    :param num_units: number of units to propose for (currently restricted to 1!)
    :param max_timepoint: events are moved to and from time window [0, max_timepoint]
    :param dtype=tf.int32: the return type of the update.
    :param name: name of the returned JointDistributionNamed.
    :returns: a JointDistributionNamed object for which `sample()` returns the id of a
              unit, distance and direction through which to move events, and number of
              events.
    """
    assert (
        num_units == 1
    ), "Calling LeftCensoredEventTimeProposal with `num_units!=1` is not supported"

    time_range = tf.range(0, max_timepoint + 1)
    stoichiometry = tf.convert_to_tensor(stoichiometry)
    transition = tf.convert_to_tensor(transition, tf.int32)
    max_events = tf.convert_to_tensor(max_events, events.dtype)

    # Identify the source and destination states for `transition`
    src_state_idx, dest_state_idx = states_from_transition_idx(
        transition, stoichiometry
    )
    dtype = events.dtype

    def unit():
        """Draw unit to update"""
        with tf.name_scope("unit"):
            return gld.UniformKCategorical(
                num_units,
                mask=ps.ones(events.shape[0], dtype=events.dtype),
                float_dtype=events.dtype,
                name="unit",
            )

    def timepoint():
        """Draw timepoint to move events to or from"""
        with tf.name_scope("timepoint"):
            return gld.UniformInteger(
                low=[0], high=[max_timepoint + 1], float_dtype=dtype
            )

    def direction():
        """Do we move events from [-\infty, 0) into [0, max_timepoint]
           or vice versa?
        0=move from past into present
        1=move from present into past
        """
        with tf.name_scope("direction"):
            return gld.UniformInteger(low=0, high=2, float_dtype=dtype)

    def num_events(unit, timepoint, direction):
        """Draw a number of events to move to/from `timepoint` in `unit`
        from the past into the present or vice versa according to `direction`.
        The number of possible events is bounded by the topology of the state
        transition model or a user-configurable `max_events`, whichever is the
        minimum.
        """
        with tf.name_scope("num_events"):
            # Events is a [num_units, num_timepoints, num_transitions] tensor.
            # We need to gather on the first 2 dimensions,
            # i.e. events[unit, time_range, :]
            unit_events = tf.gather(events, unit, axis=-3)
            unit_time_events = tf.gather(unit_events, time_range, axis=-2)
            # Initial state is a [M, S] tensor.  We gather the
            unit_initial_state = tf.gather(initial_state, unit, axis=-2)

            state = compute_state(
                unit_initial_state,
                unit_time_events,
                stoichiometry,
            )  # MxTxS

            def pull_from_past():
                r"""Choose number of A->B transition events to move from pre-history
                into the events time-window, subject to the bound
                $$
                \phi(m,t,B) = \min (1, b_1,\dots, b_T, n_{\max})
                $$
                """
                with tf.name_scope("pull_from_past"):
                    state_ = state[..., dest_state_idx]
                    state_ = _mask_max(state_, timepoint, axis=-1)
                    x_bound = tf.minimum(tf.reduce_min(state_), max_events)
                    return x_bound

            def push_to_past():
                r"""Choose number of A->B transition events to move from events
                time-window into pre-history, subject to bound
                $$
                \phi{mtA} = \min (1, a_1,\dots, a_t, y^{AB}_{mt}, n_{\max})
                $$
                """
                # Extract vector of source state values over [0, max_timepoint]
                # for the selected metapopulation
                state_ = state[..., src_state_idx]
                state_ = _mask_max(state_, timepoint, axis=-1)

                # Pushing to past is also bound by
                # events[unit, timepoint, transition]
                gather_indices = tf.concat(
                    [unit, timepoint, [transition]], axis=-1
                )
                num_available_events = tf.gather_nd(events, gather_indices)

                x_bound = tf.concat(
                    [
                        state_,
                        tf.broadcast_to(num_available_events, state_.shape),
                        tf.broadcast_to(max_events, state_.shape),
                    ],
                    axis=-1,
                )
                x_bound = tf.reduce_min(x_bound)

                return x_bound

            x_bound = tf.cond(
                direction == 0, true_fn=pull_from_past, false_fn=push_to_past
            )

            return gld.UniformInteger(
                low=[1], high=[x_bound + 1], float_dtype=dtype
            )

    return tfd.JointDistributionNamed(
        dict(
            unit=unit,
            timepoint=timepoint,
            direction=direction,
            num_events=num_events,
        )
    )
